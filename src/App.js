import './App.scss';
import Header from './components/header';
import About from './components/about';

function App() {
  return (
    <div className="App">
      <Header />
      <main>
        <About />
      </main>
    </div>
  );
}

export default App;
