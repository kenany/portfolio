/* eslint-disable testing-library/no-render-in-setup */
import { render, screen } from '@testing-library/react';
import About from '../components/about';

describe('About', () => {
    
    beforeEach(() => {
        render(<About />);
    });

    it('should renders avatar', () => {
        const img = screen.getByTestId('avatar');
        expect(img).toBeInTheDocument();
        expect(img).toBeInstanceOf(HTMLImageElement);
    });
      
    it('should renders NYOBE KENDECK', () => {
        const name = screen.getByTestId('name');
        expect(name).toBeInTheDocument();
        expect(name.textContent).toContain('NYOBE KENDECK')
    });

    it('should renders Armel', () => {
        const surname = screen.getByTestId('surname');
        expect(surname).toBeInTheDocument();
        expect(surname.textContent).toContain('Armel')
    });

    it('should renders email', () => {
        const email = screen.getByTestId('email');
        expect(email).toBeInTheDocument();
        expect(email.textContent).toContain('kendecknyobe@yahoo.fr')
    });

});
