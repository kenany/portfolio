/* eslint-disable testing-library/no-render-in-setup */
import { render, screen } from '@testing-library/react';
import SkillRating from '../components/skill-rating';

describe('SkillRating', () => {

    const label = 'Java';
    const value = 90;
    
    beforeEach(() => {
        render(<SkillRating label={label} value={value} />);
    });

    it('should renders label', () => {
        const labelNode = screen.getByTestId('label');
        expect(labelNode).toBeInTheDocument();
        expect(labelNode.textContent).toContain(label);
    });
      
    it('should renders bar', () => {
        const barNode = screen.getByTestId('bar');
        expect(barNode).toBeInTheDocument();
        expect(barNode.style.width).toBe(value + '%');
    });

});
