/* eslint-disable testing-library/no-render-in-setup */
import { render, screen } from '@testing-library/react';
import Header from '../components/header';

describe('Header', () => {
    
    beforeEach(() => {
        render(<Header />);
    })

    it('should render Image', () => {
        const img = screen.getByTestId('cover');
        expect(img).toBeInTheDocument();
        expect(img).toBeInstanceOf(HTMLImageElement);
    });

    it('should render navbar', () => {
        const nav = screen.getByTestId('navbar');
        expect(nav).toBeInTheDocument();
        expect(nav.classList.contains('nav')).toBeTruthy();
    });

    it('should render banner', () => {
        const banner = screen.getByTestId('banner');
        expect(banner).toBeInTheDocument();
        expect(banner.classList.contains('banner')).toBeTruthy();
    });

});
