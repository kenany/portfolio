import { render, screen } from '@testing-library/react';
import App from '../App';

test('renders Kenany', () => {
  render(<App />);
  const linkElement = screen.getByText(/Kenany/i);
  expect(linkElement).toBeInTheDocument();
});
