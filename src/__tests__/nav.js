/* eslint-disable testing-library/no-render-in-setup */
import { render, screen } from '@testing-library/react';
import Nav from '../components/nav';

describe('Nav', () => {
    
    beforeEach(() => {
        render(<Nav />);
    })

    it('should render menu', () => {
        const menu = screen.getByTestId('menu');
        expect(menu).toBeInTheDocument();
        expect(menu).toBeInstanceOf(HTMLUListElement);
        expect(menu.classList.contains('menu')).toBeTruthy();
    });

    it('should render Kenany', () => {
        const brand = screen.getByText(/Kenany/i);
        expect(brand).toBeInTheDocument();
        expect(brand).toBeInstanceOf(HTMLAnchorElement);
    });

});