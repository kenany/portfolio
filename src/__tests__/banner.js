/* eslint-disable testing-library/no-render-in-setup */
import { render, screen } from '@testing-library/react';
import Banner from '../components/banner';

describe('Banner', () => {
    
    beforeEach(() => {
        render(<Banner />);
    });

    it('should renders I am Armel NYOBE', () => {
        const h1 = screen.getByTestId('title');
        expect(h1).toBeInTheDocument();
        expect(h1.textContent).toContain('Armel NYOBE');
    });
      
    it('should renders Fullstack developer', () => {
        const h2 = screen.getByTestId('subtitle');
        expect(h2).toBeInTheDocument();
        expect(h2.textContent).toContain('Fullstack developer')
    });

});

