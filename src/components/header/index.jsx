import React from 'react';
import Nav from '../nav';
import Banner from '../banner';

import style from './header.module.scss';
import banner from '../../assets/images/banner.jpg'


const Header = () => {
    return (
        <header className={style.header}>
            <img src={banner} alt="banner" data-testid="cover" />
            <Nav />
            <Banner />
        </header>
    );
};

export default Header;