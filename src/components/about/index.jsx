import React from 'react';
import SkillRating from '../skill-rating';

import style from './about.module.scss';
import armel from '../../assets/images/armel.png';
import skills from '../../assets/data/skills.json';


const About = () => {

    return (
        <div className={style.about}>

            <section className={style.container}>


                <aside className={style.left}>

                    <div className={style.details}>
                        <div className={style.detailsAvatar}>
                            <figure className={style.avatarContainer}>
                                <img src={armel} alt="Armel Nyobe" data-testid='avatar' className={style.avatar} />
                            </figure>
                        </div>
                        <div className={style.detailsInfo} itemType="https://schema.org/Person">

                            <div className={style.item}>
                                <span className={style.label + ' text-bold'}>Name: </span>
                                <span itemProp='name' className={style.value}>
                                    <strong data-testid="name">NYOBE KENDECK</strong> 
                                </span>
                            </div>

                            <div className={style.item}>
                                <span className={style.label + ' text-bold'}>Surname: </span>
                                <span itemProp='surname' data-testid="surname" className={style.value}>Armel</span>
                            </div>

                            <div className={style.item}>
                                <span className={style.label + ' text-bold'}>Email: </span>
                                <span itemProp='email' data-testid="email" className={style.value}>kendecknyobe@yahoo.fr</span>
                            </div>

                            <div className={style.item}>
                                <span className={style.label + ' text-bold'}>Tel: </span>
                                <span itemProp='phone' className={style.value}>(+237) 674 - 160 - 948</span>
                            </div>

                        </div>
                    </div>

                    <div className={style.skills}>
                    <h2 className={style.title} aria-label='Armel Nyobe skills'> My skills </h2>
                        {skills.map((skill, i) => <SkillRating label={skill.label} value={skill.value} key={i} />)}
                    </div>
                    
                </aside>

                <article className={style.rigth}>
                    <header>
                        <h2 aria-label='about Armel Nyobe'> About Me </h2>
                    </header>

                    <p>
                        The first time I heard about algorithms and programming is in 2012 when I was at the college.
                        It was weird for me, but my brother advised me to follow that path. Then I started to learning writting
                         algorithms myself at the holidays. I found AlgoBox, I used it to check the trueness of my algorithms. So I decided to do 
                         software engineering at university.
                    </p>

                    <p>
                        At the university, I started with computer science. I learned more about algorithms and programming langages. 
                        I noticed that I was really confortable in programming. I tried many langages, I decided to evolve in Java because I found 
                        it more representative of OOP concepts and Javascript for the fun.
                    </p>

                    <p>
                        I started my first intership in 2015. I was learning AngularJS (because I had to use it in enterprise). I was really difficult for me 
                        It was really difficult for me because I didn't understand well the concepts like dependency injection, two ways binding, MVC was new for 
                        me and I never made a routing configurate. But now I can use Spring...
                    </p>
                    
                </article>
                
            </section>
        </div>
    );
};

export default About;