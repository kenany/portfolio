import React from 'react';

import style from './skill-rating.module.scss';

/**
 * 
 * @param {{label: string, value: number}} props 
 * @returns 
 */
const SkillRating = (props) => {
    return (
        <div className={style.rating}>

            <div className={style.label} data-testid='label'>{props.label + ' ' + props.value + '%'}</div>
            <div className={style.bar}>
                <div className={style.barRate} data-testid='bar' style={{width: props.value + '%'}}></div>
            </div>
            
        </div>
    );
};

export default SkillRating;