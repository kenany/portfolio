/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import style from './nav.module.scss'

const Nav = () => {

    const items = [
        {label: 'Home', link: '#'},
        {label: 'About', link: '#'},
        {label: 'Work', link: '#'},
        {label: 'Skills', link: '#'},
    ];

    const Item = ({data: {label, link}}) => {
        return <li>
            <a className='text-ligth' href={link}> {label} </a>
        </li>
    }

    return (
        <nav className={'text-bold ' + style.nav} data-testid='navbar'>

            <a href='#' className='text-ligth'>Kenany</a>
            <ul className={style.menu} data-testid="menu">
                {items.map((item, i) => <Item data={item} key={i}/>)}
            </ul>
            
        </nav>
    );
};

export default Nav;