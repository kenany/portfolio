import React from 'react';

import style from './banner.module.scss'

const Banner = () => {
    return (
        <section className={'text-ligth ' + style.banner} data-testid='banner'>
            <h1 data-testid="title">I am Armel NYOBE</h1>
            <h2 data-testid="subtitle">Fullstack developer</h2>
        </section>
    );
};

export default Banner;